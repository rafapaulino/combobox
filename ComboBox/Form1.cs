﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ComboBox
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            adicionaMulher();
        }

        private void adicionaMulher()
        {
            string mulher = this.textBox1.Text;
            this.textBox1.Text = "";
            comboBox1.Items.Add(mulher);
         }

        private void limpar()
        {
            comboBox1.Items.Clear();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            limpar();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (comboBox1.Items.Count > 0 )
            {
                string resultado = comboBox1.Items[2].ToString();
                MessageBox.Show("O Terceiro Elemento é: " + resultado);
            }
            else
            {
                MessageBox.Show("O Combo está vazio!");
            }
        }
    }
}
